DomainSearch

This is an Eclipse project of type "Tomcat Project" created by the Eclipse Tomcat plugin available at the below URL...
https://sourceforge.net/projects/tomcatplugin/files/latest/download?source=files

Once this plugin is successfully installed and configured to work with the local instance of Tomcat (version 8.5.x) any changes made the project will result in the automatic building of a WAR file and the subsequent deployment of said WAR file to Tomcat.

The developer can also choose to manually export the WAR file via the below steps...
1. Right click on the project root node in the package explorer.
2. Select "Tomcat Project > Export to the WAR file sets in project properties".

NOTE: The project is currently set up to export to a path that is specific to my machine and this needs to be modified prior to manual exporting in order for it to succeed.

Once deployed to Tomcat, the web app is accessible via the below URL:
http://localhost:8080/DomainSearch/
