/**
 * 
 */
package listeners;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Scanner;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import db.DomainDb;

/**
 * The listener class which will facilitate the loading the domain DB into memory.
 * 
 * @author jayfuerstenberg
 */
public class ApplicationStartUpListener implements ServletContextListener {
	
	private static final String DB_INSTANCE = "__domain_db__";
	
	private void lazyLoadDb(String path, DomainDb db) {
		
		try {
			FileInputStream fis = new FileInputStream(path);
			Scanner scanner = new Scanner(fis);
			
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				int comma = line.indexOf(",");
				String domain = line.substring(comma + 1);
				
				db.addEntry(domain);
			}
			
			scanner.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public ApplicationStartUpListener() {
	}

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		
		// Release the domain DB.
		ServletContext sc = event.getServletContext();
		sc.removeAttribute(DB_INSTANCE);
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		
		// Load the domain name DB now...
		ServletContext sc = event.getServletContext();
		DomainDb db = (DomainDb) sc.getAttribute(DB_INSTANCE);
		if (db == null) {
			String path = sc.getRealPath("/WEB-INF/resources/top-1m.csv");
			
			db = new DomainDb();
			lazyLoadDb(path, db);
			sc.setAttribute(DB_INSTANCE, db);
		}
	}

}
