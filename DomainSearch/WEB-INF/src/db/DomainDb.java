/**
 * 
 */
package db;

import java.util.ArrayList;
import java.util.TreeMap;

/**
 * The in-memory database for storing domain names.
 * Internally it uses a trie for optimal storage of domain names.
 * 
 * @author jayfuerstenberg
 */
public class DomainDb {
	
	/**
	 * The shortest valid search/added term length.
	 */
	private static final int MIN_TERM_LENGTH = 3;
	
	
	/**
	 * Nodes with the DB.
	 */
	public class Node {
		/**
		 * The character of this node.
		 */
		public Character character;
		
		public TreeMap<Character, Node> children = new TreeMap<Character, Node>();
		
		/**
		 * The flag denoting whether this node is the last in a series.
		 * If true, it represents the last character in a search term or added entry.
		 */
		public boolean isEnd;
		
		public Node() {
			this.character = (char) -1;
		}
		
		/**
		 * Constructor.
		 * 
		 * @param character
		 */
		public Node(char character) {
			
			this.character = character;
		}
		
		public Node getChildWithCharacter(Character character) {
			
			return children.get(character);
		}
		
		public void optionallyAddChild(Node child) {
			
			if (children.get(child.character) != null) {
				// Already exists.  No need to add.
				return;
			}
			
			children.put(child.character, child);
		}
		
		public String toString() {
			
			return character.toString();
		}
		
		public void print(int indentation) {
			
			StringBuffer sb = new StringBuffer();
			while (indentation-- >= 0) {
				sb.append(" ");
			}
			
			sb.append(character);
			if (isEnd) {
				sb.append(" [end]");
			}
			System.out.println(sb.toString());
		}
	}
	
	/**
	 * The root node of the underlying trie.
	 */
	public Node rootNode = new Node();
	
	/**
	 * Adds the provided entry to the DB.
	 * 
	 * @param entry
	 */
	public void addEntry(String entry) {
		
		if (entry == null) {
			// Ignore the invalid entry.
			return;
		}
		
		int length = entry.length();
		if (entry.length() == 0) {
			return;
		}
		
		Node currentNode = rootNode;
		
		for (int index = 0; index < length; index++) {
			char c = entry.charAt(index);
			Node existingChild = currentNode.getChildWithCharacter(c);
			if (existingChild == null) {
				// The child doesn't exist yet.  Add it!
				existingChild = new Node(c);
				currentNode.optionallyAddChild(existingChild);
			}
			
			currentNode = existingChild;
		}
		
		currentNode.isEnd = true;
	}
	
	public ArrayList<String> getEntriesStartingWithText(String text) {
		
		if (text == null) {
			return null;
		}
		
		int length = text.length();
		if (length < MIN_TERM_LENGTH) {
			// Not long enough of a search term!
			return null;
		}
		
		ArrayList<String> results = new ArrayList<String>();
		
		// Find the node in the trie which matches the search term...
		Node currentNode = rootNode;
		
		for (int index = 0; index < length; index++) {
			char c = text.charAt(index);
			Node matchingNode = currentNode.getChildWithCharacter(c);
			if (matchingNode == null) {
				// No such entry!
				return null;
			}
			
			currentNode = matchingNode;
		}
		
		// NOTE: At this point, currentNode is the node from which all child entries should be discovered.
		discoverAllEntriesStemmingFrom(currentNode, text, results);
		return results;
	}
	
	public void discoverAllEntriesStemmingFrom(Node node, String prefix, ArrayList<String> results) {
		
		if (node == null) {
			return;
		}
		
		if (node.isEnd) {
			results.add(prefix);
			return;
		}
		
		for (Node child : node.children.values()) {
			String entry = prefix + child.character;
			discoverAllEntriesStemmingFrom(child, entry, results);
		}
	}
}
