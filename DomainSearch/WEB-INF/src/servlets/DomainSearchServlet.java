/**
 * 
 */
package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import db.DomainDb;

/**
 * The servlet which acts as the interface for this single page web application.
 * 
 * @author jayfuerstenberg
 */
public class DomainSearchServlet extends HttpServlet {

	private static final long serialVersionUID = 8024871834046558619L;

	private static final String DB_INSTANCE = "__domain_db__";

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException {
		
		// GETs are for displaying the single web page of this application
		ServletContext sc = this.getServletContext();
		RequestDispatcher jsp = sc.getRequestDispatcher("/WEB-INF/index.jsp");
		jsp.forward(request, response); 
	}   

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		// POSTs are for searching against a given search term and responding with a newline-delimited list of results.
		String searchTerm = request.getHeader("x-searchterm");
		String type = request.getHeader("x-type");
		boolean forSuggestion = (type != null && type.equals("suggestion"));

		// Get the DB, hopefully loaded by now...
		ServletContext sc = this.getServletContext();
		DomainDb db = (DomainDb) sc.getAttribute(DB_INSTANCE);

		if (db != null) {
			ArrayList<String> results = db.getEntriesStartingWithText(searchTerm);
			if (results != null) {
				int count = 0;
				for (String result : results) {
					response.getWriter().write(result);
					response.getWriter().write("\n");

					if (forSuggestion) {
						// The suggestion dropdown can only display so many options so consider performance of both the server and client now...
						count++;
						if (count == 50) {
							break;
						}
					}
				}
			}
		}
	}
}
