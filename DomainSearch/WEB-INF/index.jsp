<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Domain Search</title>
</head>
<body>

<!-- The list of suggestions dependent on the input within the search term field -->
<datalist id="suggestions">
</datalist>

<div id="root">
	<div id="search">
		<form name="search-form" action="" onkeypress="return event.keyCode != 13;">
			<input id="search-term" list="suggestions" onKeyUp="optionallyPerformSuggestions(event);" />
			<input type="button" value="Submit" onClick="performSearch()" />
		</form>
	</div>
	<br/>
	<div id="results" />
</div>
</body>

<!-- The JS that drives the page behavior appears here -->
<script>

var searchOperation;

function optionallyPerformSuggestions(event) {

	// Prevent the server from being flooded with suggestion requests if the user is typing fast.
	clearTimeout(searchOperation);
	searchOperation = setTimeout(performSuggestions, 300);
}

function performSuggestions() {
	
	var suggestionsNode = document.getElementById("suggestions");
	
	var searchTerm = document.getElementById("search-term").value;
	
	if (searchTerm.length < 3) {
		var suggestionsNode = document.getElementById("suggestions");
        suggestionsNode.innerHTML = "";
		return;
	}
		
	var request = new XMLHttpRequest();
	
    request.onreadystatechange = function() {
        if (request.readyState == XMLHttpRequest.DONE) {
           if (request.status == 200) {

           		var results = request.responseText.split("\n");
           		
           		// Add a link for each entry in the results...
           		results.forEach(function(element) {
           			var option = document.createElement("option");
           			option.value = element;
					suggestionsNode.appendChild(option);
           		});
           }
        }
    };

    request.open("POST", "search", true);
    request.setRequestHeader("x-searchterm", searchTerm);
    request.setRequestHeader("x-type", "suggestion");
    request.send();
    
    suggestionsNode.innerHTML = "";
}

function isTextInSuggestions(text) {
	
	var options = document.getElementById("suggestions").options;
	for (var i = 0; i < options.length; i++) {
		if (text == options[i].value) {
			return true;
		}
	}
	
	return false;
}

function performSearch() {

	var resultsNode = document.getElementById("results");
	var searchTerm = document.getElementById("search-term").value;
	if (isTextInSuggestions(searchTerm)) {
		// Navigate to that domain!
		var link = "http://" + searchTerm;
		var win = window.open(link, '_blank');
  		win.focus();
  		return;
	}
		
	// Perform actual search against the search term...
	var request = new XMLHttpRequest();
	
    request.onreadystatechange = function() {
        if (request.readyState == XMLHttpRequest.DONE) {
           if (request.status == 200) {

           		if (request.responseText.length == 0) {
           			var text = document.createTextNode("No matches found");
           			resultsNode.appendChild(text);
           		} else {
           		
           			var results = request.responseText.split("\n");
 
           			// Add a link for each entry in the results...
           			results.forEach(function(element) {
           				var item = document.createElement("div");
           				var text = document.createTextNode(element);
           				var link = document.createElement("a");
           				link.href = "http://" + element;
           				link.title = element;
           				link.target = "_blank";
           				link.appendChild(text);
           				item.appendChild(link);
           				resultsNode.appendChild(item);
           			});
           		}
           }
        }
    };

    request.open("POST", "search", true);
    request.setRequestHeader("x-searchterm", searchTerm);
    request.send();
    
  	resultsNode.innerHTML = "";
}

</script>
</html>